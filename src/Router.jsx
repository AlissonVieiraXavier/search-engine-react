import {
    createBrowserRouter,
    createRoutesFromElements,
    Route,
  } from "react-router-dom";

import Home from "./pages/Home";
import Film from "./pages/Film";

export const Router = createBrowserRouter(
    createRoutesFromElements(  
        <>
              <Route path="/" element={<Home/>}/>
              <Route path="film" element={<Film/>} >
                <Route path=":id_film"/>
              </Route> 
        </>    
    )
  );
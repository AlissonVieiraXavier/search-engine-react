//http://www.omdbapi.com/?s=trapalhoes&r=json&page=1&v=1&apikey=887aa25c&plot=full&type
import axios from 'axios';


function BuilderGetRequest(parameters){
    const baseUrl = process.env.REACT_APP_API_URL

    let urlReq = ''   
    let parametersReq = ''

    parameters.forEach(item => {
        parametersReq = parametersReq + item.nameParameter + '=' + item.value + '&';
    });
    urlReq = baseUrl + '?' + parametersReq.slice(0, -1); // Remover o último &

    return urlReq
};

function GetRequisition(url){
    return axios.get(url)
    .then(response => {
        return response.data;
    })
    .catch(e => {
        console.error(e)
        throw e
    })
};

async function GetSearch(type,searchContent){
    const key = process.env.REACT_APP_API_KEY
    
    let parameterReq = [];

    if(type == 'general'){
        parameterReq = [
            { nameParameter: 's', value: searchContent },
            { nameParameter: 'r', value: 'json' },
            { nameParameter: 'page', value: 1 },
            { nameParameter: 'v', value: 1 },
            { nameParameter: 'apikey', value: key },
            { nameParameter: 'plot', value: 'full' },
            { nameParameter: 'type', value: '' },
        ];
    }

    if(type == 'byId'){
        parameterReq = [
            { nameParameter: 'i', value: searchContent },
            { nameParameter: 'apikey', value: key }
        ];
    }

    let urlRequest = BuilderGetRequest(parameterReq)

    try{
        let response = await GetRequisition(urlRequest)
        return response;
    }catch(e){
        console.error(e)
    }
};


export default GetSearch;

import './index.css'

export default function AuthsIcons() {

    return(
        <div className="container-auths">
                    <div className="auths-options">Login</div>
                    <div className="auths-options_r">Register</div>
        </div>
    )
};

import { Col, Container, Row } from "react-grid-system";
import noPhoto from '../../assets/sem-foto.gif';
import './index.css'
import { FaStar } from "react-icons/fa";



export default function DadosFilm(props) {

    const { Film } = props;

    console.log(Film)

    return(
        <Container className="container_dados_film">
            <Row sm={11} md={12} lg={12}>
                <Col sm={12} md={4}>
                    <div className='image_col'>
                        <img src={Film.Poster && Film.Poster !== "N/A" ? Film.Poster : noPhoto} alt="Poster"/>
                    </div>
                    <div className="rating"><FaStar />  {Film.Ratings ? Film.Ratings[0]?.Value : null}</div>
                </Col>
                <Col sm={12} md={7}>
                    <div className="details_col">
                        <h1 className="details_col_title">{Film.Title}</h1>
                        <div className="description">
                            {Film.Plot}
                        </div>
                        <div className="details_col_infos">
                            <h3>Details:</h3>
                            {Object.entries(Film).map(([key, value]) => (
                                value.toString() !== "N/A" && 
                                value.toString() !== Film.Poster &&
                                value.toString() !== Film.Plot && 
                                value.toString() !== Film.Title &&
                                value.toString() !== Film.Response &&
                                !Array.isArray(value) ? (
                                    <div className="details_col_info" key={key}>
                                        <h5>{key}:</h5> 
                                        <p>
                                            {value.toString()}
                                        </p>
                                    </div>
                                ) : null
                            ))}
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
    )
};

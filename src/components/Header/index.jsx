import { Col, Container, Row } from "react-grid-system"
import './index.css';


export default function Header() {
    return(
         <Container >
            <Row>
                <Col className="title" sm={12} md={6}>
                    Most complete movie information search engine
                </Col>
                <Col className="subtitle" sm={12} md={5}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam pulvinar erat in arcu tempor bibendum. Donec molestie quam ligula, et blandit diam scelerisque at. Cras quis auctor dui.
                </Col>
            </Row>
         </Container>)
};
import './index.css'
import Logotipo from '../../assets/logodef.png'
import SearchCamp from '../SeachCamp'
import AuthsIcons from '../AuthsIcons'
import { Link, useLocation } from 'react-router-dom'

export default function Navbar() {

    const location = useLocation();

    return(
        <div className='navbar-container'>
            <div className='logo'> 
                <img src={Logotipo} alt="logotipo"/>
            </div>
            <div className='auths'>
                <AuthsIcons/>
            </div>
            <div className='search-fild'>
                { 
                    location.pathname.startsWith('/film') ? 
                    (
                        <>
                            <Link to='/' className='link_back_to_home'>
                                Come back to HomePage
                            </Link>
                        </>
                    ) : 
                    (
                        <>
                            <SearchCamp/>
                        </>
                    )
                }
                
            </div>
        </div>
    )
};

import { TbPlayerTrackNext } from "react-icons/tb";
import { TbPlayerTrackPrev } from "react-icons/tb";
import { useDispatch, useSelector } from "react-redux";
import { setaAtualPage } from "../../Store/Slices/pagination";
import './index.css';

export default function PageCounter() {
    
    const currentPage = useSelector(state => state.pagination.actualPage);
    const totalPages = useSelector(state => state.pagination.totalPagesResult);
    const dispatch = useDispatch();

    const handlePrevious = () => {
      if (currentPage > 1) {
        dispatch(setaAtualPage(currentPage - 1));
      }
    };
  
    const handleNext = () => {
      if (currentPage < totalPages) {
        dispatch(setaAtualPage(currentPage + 1));
      }
    };
    const getPageNumbers = () =>{
        const numbers = []
        const maxPagesToshow = 5

        let startPage = Math.max(1,currentPage - Math.floor(maxPagesToshow / 2))
        let endPage = Math.min(totalPages, startPage + maxPagesToshow - 1)

        // Garante que o número total de páginas exibidas seja igual a maxPagesToShow e evitar bug
        if (endPage - startPage + 1 < maxPagesToshow) {
            startPage = Math.max(1, endPage - maxPagesToshow + 1);
        }
      
        for (let i = startPage; i <= endPage; i++) {
            numbers.push(i);
        }
          return numbers;
        };
    
  
    return (
      <div className="container_page_counter">
        <button  className={currentPage === 1 ? 'container_page_counter_dis' : 'container_page_counter_button'} onClick={handlePrevious} disabled={currentPage === 1}>
            <TbPlayerTrackPrev />
        </button>
        {getPageNumbers().map((number) => (
            <span key={number} className={number == currentPage ? 'container_page_counter_span_active':'container_page_counter_span'}>
                <button onClick={() => dispatch(setaAtualPage(number))}>{number}</button>
            </span>
        ))}
        <button className={currentPage === totalPages ? 'container_page_counter_dis' : 'container_page_counter_button'} onClick={handleNext}>
            <TbPlayerTrackNext />
        </button>
      </div>
    );
  };
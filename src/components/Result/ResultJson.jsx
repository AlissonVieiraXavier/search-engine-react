const ResultJson = {
    "Search": [
        {
            "Title": "O Casamento dos Trapalhões",
            "Year": "1988",
            "imdbID": "tt0139102",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BMTBiOTg4YmItNjU0Yy00NzlhLTk1ZjktNjQ3Y2M0MWEzZTQ1XkEyXkFqcGdeQXVyOTU3ODk4MQ@@._V1_SX300.jpg"
        },
        {
            "Title": "Os Saltimbancos Trapalhões",
            "Year": "1981",
            "imdbID": "tt0138073",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BMThkN2RjNzAtMGIyMC00YzczLWJiZjgtMDgxYTM4ZTcwNjVmXkEyXkFqcGdeQXVyNTU3MTY2Mg@@._V1_SX300.jpg"
        },
        {
            "Title": "Os Trapalhões no Auto da Compadecida",
            "Year": "1987",
            "imdbID": "tt0139660",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BMTE3MDYwNDktM2I3YS00ODBhLWJiNWItNjA3MDg1MDgzZTE5XkEyXkFqcGdeQXVyNTU3MTY2Mg@@._V1_SX300.jpg"
        },
        {
            "Title": "Uma Aventura do Zico",
            "Year": "1998",
            "imdbID": "tt0140656",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BNzQ0NWE2ZmQtMzI5YS00M2ZlLWE5YTgtYmRhNTBmODliMDlkXkEyXkFqcGdeQXVyNzMwOTY2NTI@._V1_SX300.jpg"
        },
        {
            "Title": "A Princesa Xuxa e os Trapalhões",
            "Year": "1989",
            "imdbID": "tt0121654",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BNDMwYzQ2YWMtODZkNS00YjdiLWIyMzYtZWZhMjMyZDdkNGMyXkEyXkFqcGdeQXVyNDUxNjc5NjY@._V1_SX300.jpg"
        },
        {
            "Title": "Os Trapalhões e a Árvore da Juventude",
            "Year": "1991",
            "imdbID": "tt0207166",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BZTM3ZWM4YzYtM2ExYi00M2FmLTg1NTUtZWUyODhhZjU5M2FkXkEyXkFqcGdeQXVyNDUxNTE1Nzc@._V1_SX300.jpg"
        },
        {
            "Title": "Os Trapalhões na Terra dos Monstros",
            "Year": "1989",
            "imdbID": "tt0139659",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BMGQxZGQxOTctZWNmYy00ZTQ1LWI1NDAtMTM3ZTUzM2Y4Y2Y2XkEyXkFqcGdeQXVyNDUxNjc5NjY@._V1_SX300.jpg"
        },
        {
            "Title": "Pele: Birth of a Legend",
            "Year": "2016",
            "imdbID": "tt0995868",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BMTg2NzQwMzQyMF5BMl5BanBnXkFtZTgwNzkzODk2ODE@._V1_SX300.jpg"
        },
        {
            "Title": "Pele: Birth of a Legend",
            "Year": "2016",
            "imdbID": "tt0995868",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BMTg2NzQwMzQyMF5BMl5BanBnXkFtZTgwNzkzODk2ODE@._V1_SX300.jpg"
        },
        {
            "Title": "O Outro Lado Do Atleta José Aldo",
            "Year": "2012",
            "imdbID": "tt9303462",
            "Type": "movie",
            "Poster": "https://m.media-amazon.com/images/M/MV5BMjgxZDMyZGQtMGQyZC00MjQxLTlmMGYtNDBmNTRhZjE2Mzg5XkEyXkFqcGdeQXVyNzk3MDI2Nzc@._V1_SX300.jpg"
        }
    ],
    "totalResults": "10",
    "Response": "True"
}

export default ResultJson;
import { useState } from 'react'
import './index.css'
import { setNoMoreSearch, setResult, setSearching } from '../../Store/Slices/search';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../Button';
import GetSearch from '../../Services/SearchService';
import { setaAtualPage } from '../../Store/Slices/pagination';

export default function SearchCamp() {

    const [searchContent, setSearchContent] = useState("");
    //const loadingScreen = useSelector(state => state.search.searching);
    const dispatch = useDispatch();

    const HandleChangeInput = (value) =>{
        setSearchContent(value)
    }
    const HandleSubmitInput = async () =>{
        dispatch(setSearching())
        let result = await GetSearch('general',JSON.stringify(searchContent))
        dispatch(setResult(result))
        dispatch(setaAtualPage(1))
        //setNoMoreSearch()
    }
    const handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            HandleSubmitInput();
        }
    };

    return(
        <div className="search_camp_container">
            <div className='field'>
                <input 
                    className='field_input'
                    type="text" 
                    placeholder="Pesquise por filmes e relacionados"
                    onChange={(e) => HandleChangeInput(e.target.value)}
                    onKeyDown={handleKeyPress}
                />
            </div>
            <div 
                className='btn'
                onClick={HandleSubmitInput}
                >
                  <Button
                    width="100px"
                    height="45px"
                  >
                    Search
                  </Button>
            </div>
        </div>
    )
};

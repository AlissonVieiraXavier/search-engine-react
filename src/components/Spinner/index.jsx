
import './index.css';

export default function Spinner() {

    const textWhileLoading = "Searching";
    const arrayText = textWhileLoading.split('');

    return(
        <div className='container_spinner'>
            <div className="loading_container">
                <div className="loading_text">
                    {
                        arrayText.map((item, index)=>(
                            <span key={index}>{item}</span>
                        )) 
                    }
                </div>
            </div>
        </div>
    )
};

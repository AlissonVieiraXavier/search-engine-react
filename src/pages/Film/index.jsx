import { useParams } from "react-router-dom"
import Navbar from "../../components/Navbar";
import { useEffect } from "react";
import GetSearch from "../../Services/SearchService";
import { useState } from "react";
import DadosFilm from "../../components/DadosFilm";


export default function Film() {
    
    const {id_film} = useParams();
    const [film, setFilm] = useState({});

    useEffect( () => {
        const GetReturnAPI = async () => {
            try{
                let returnAPI = await GetSearch('byId',id_film)
                setFilm(returnAPI)
            }catch(error) {
                console.error(error)
            }

        }
        GetReturnAPI()
    }, [id_film]);
    
    return(
    <div>
        <Navbar/>
        <DadosFilm Film={film}/>
    </div>)
};
/*

Actors
: 
"Renato Aragão, José de Abreu, Tatiana Delamare"
Awards
: 
"N/A"
BoxOffice
: 
"N/A"
Country
: 
"Brazil"
DVD
: 
"N/A"
Director
: 
"José Alvarenga Jr."
Genre
: 
"Family, Comedy"
Language
: 
"Portuguese"
Metascore
: 
"N/A"
Plot
: 
"A misogynist and shabby farmer, brother of other three goofy hillbillies leaves to the big city searching for a wife and wins the heart of a hot tempered woman."
Poster
: 
"https://m.media-amazon.com/images/M/MV5BMTBiOTg4YmItNjU0Yy00NzlhLTk1ZjktNjQ3Y2M0MWEzZTQ1XkEyXkFqcGdeQXVyOTU3ODk4MQ@@._V1_SX300.jpg"
Production
: 
"N/A"
Rated
: 
"N/A"
Ratings
: 
[{…}]
Released
: 
"14 Feb 1988"
Response
: 
"True"
Runtime
: 
"82 min"
Title
: 
"O Casamento dos Trapalhões"
Type
: 
"movie"
Website
: 
"N/A"
Writer
: 
"Paulo Andrade"
Year
: 
"1988"
imdbID
: 
"tt0139102"
imdbRating
: 
"5.3"
imdbVotes
: 
"404"
*/